import { PathLike } from 'fs'
import { IDatabaseAdapterConfig } from 'database-adapter'


export interface ILokiJsDbOptions extends IDatabaseAdapterConfig {
	path: PathLike,
	schema?: string[]
}
